package com.wdjk.webdemo624.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 话题表，保存话题基本信息 前端控制器
 * </p>
 *
 * @author zhuhua
 * @since 2021-05-04
 */
@RestController
@RequestMapping("/webdemo624/topic")
public class TopicController {

}

