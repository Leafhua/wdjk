package com.wdjk.webdemo624.service;

import com.wdjk.webdemo624.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户表，保存用户相关信息 服务类
 * </p>
 *
 * @author zhuhua
 * @since 2021-05-04
 */
public interface UserService extends IService<User> {

}
