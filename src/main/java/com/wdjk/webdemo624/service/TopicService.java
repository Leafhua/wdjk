package com.wdjk.webdemo624.service;

import com.wdjk.webdemo624.entity.Topic;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 话题表，保存话题基本信息 服务类
 * </p>
 *
 * @author zhuhua
 * @since 2021-05-04
 */
public interface TopicService extends IService<Topic> {

}
